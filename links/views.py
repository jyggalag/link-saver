from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from datetime import date

from links.models import Category, Link
from links.forms import AddCategoryForm, AddLinkForm

import math

# Create your views here.

@login_required
def index(request):
    return render(request, 'links/index.html')

@login_required
def links_index(request):
    """ show link, return categories """
    links = Link.objects.all()

    links_count = len(links)
    links_per_page = 7

    categories = Category.objects.all()
    paginator = Paginator(links, links_per_page)
    page = request.GET.get('page')

    try:
        links = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        links = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        links = paginator.page(paginator.num_pages)

    category_form = add_category(request)
    link_form = add_link(request)

    today = date.today()

    pages = math.ceil(links_count / links_per_page)

    context = {'links': links, 'categories': categories, 'category_form': category_form, 'link_form': link_form, 'links_count': links_count,
        'today': today, 'pages': pages}
    return render(request, 'links/index.html', context)


@login_required
def add_category(request):
    if request.method != 'POST':
        form = AddCategoryForm()
    else:
        form = AddCategoryForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('links:links_index'))
    
    return form


@login_required
def add_link(request):
    if request.method != 'POST':
        form = AddLinkForm()
    else:
        form = AddLinkForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('links:links_index'))
        
    return form


@login_required
def update_link(request, link_id):
    link = Link.objects.get(id=link_id)

    if link.starred == False:
        link.starred = True
        link.save()
    else:
        link.starred = False
        link.save()

    return HttpResponseRedirect(reverse('links:links_index'))


@login_required
def delete_link(request, link_id):
    link = Link.objects.get(id=link_id)
    link.delete()
    return HttpResponseRedirect(reverse('links:links_index'))