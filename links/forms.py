from django import forms

from links.models import Category, Link

class AddCategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ['name']
        labels = {'name': 'Category name'}


class AddLinkForm(forms.ModelForm):
    class Meta:
        model = Link
        fields = ['title', 'category', 'reference']