from django.db import models

# Create your models here.

class Category(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200)

    class Meta:
        verbose_name_plural = 'categories'


    def __str__(self):
        return self.name


class Link(models.Model):
    id = models.AutoField(primary_key=True)
    category = models.ForeignKey(Category)
    title = models.CharField(max_length=200)
    starred = models.BooleanField(default=False)
    date_added = models.DateTimeField(auto_now_add=True)
    reference = models.CharField(max_length=255)

    class Meta:
        verbose_name_plural = 'links'
        ordering = ('-starred', '-date_added')

    def __str__(self):
        return self.title