from django.conf.urls import url

from . import views

urlpatterns = [
    # url(r'^$', views.index, name='index'),
    url(r'^$', views.links_index, name='links_index'),
    url(r'^add_category/', views.add_category, name='add_category'),
    url(r'^add_link/', views.add_link, name='add_link'),
    url(r'^(?P<link_id>\d+)/update_link/', views.update_link, name='update_link'),
    url(r'^(?P<link_id>\d+)/delete_link/', views.delete_link, name='delete_link'),
]