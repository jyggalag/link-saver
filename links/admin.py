from django.contrib import admin

from links.models import Category, Link

# Register your models here.
admin.site.register(Category)
admin.site.register(Link)
